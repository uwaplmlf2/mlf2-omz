#!/bin/bash
#
# Convenience script to build all of the missions.
#

target=${1-dist}

[ -d =build ] || mkdir =build
for m in pugetsoundballast hurr08
do
    echo $m > MTYPE
    pushd =build
    ../configure
    make clean all $target
    popd
done

    
