/* arch-tag: c7b8c6b2-2c2c-47bd-be7c-2f32e74f9d14 */
/* DOGEE-II GAS MISSION
*/

/* Subroutine to handle mode changes
both routine and due to acoustic commands
PARAMETERS set Initialize AESOP.c */

int next_mode(int nmode, double day_time)
{
	double x;
	static int icall=0; 
	static double save10=0;
	
	
	/* %%%%%%%%%%%%%  DOGEE MISSION %%%%%%%%%%%%%% */
	
	/* real mode switching */
	if (nmode==MODE_START){
		log_event("DOGEE Mission Start\n");
		nmode=MODE_PROFILE_DOWN;
		icall=0;
		return(nmode);
	} 
	else if (nmode==MODE_ERROR){ /* this should not happen since errors cause COMM */
		log_event("ERROR MODE at wierd time- reset loop at COMM\n");
		icall=0;  /* reset loop */	
	}
else if (nmode==MODE_COMM && icall%14 !=4 && icall%14 !=6){  /* Error or Return from error */
log_event("Comm at wrong time - reset mode loop mode\n");
icall=0;  /* reset loop */
}

printf("NB Mode %d ->",nmode);

switch(icall%14){
	case 0: nmode=MODE_PROFILE_DOWN; 
		Down.Sigmax=2000.; /* don't use density on first down - later */
		break;
	case 1: nmode=MODE_PROFILE_UP;
		save1=Up.Brate;
		Up.Brate=0.;   /* turn off speed control */
		break;
	case 2: nmode=MODE_PROFILE_UP;break;  /* no need, no harm */
	case 3: nmode=MODE_COMM;  /* first COMM - get GPS and send QL */
		commhome=2;   /* no homing */
		break;   
	case 4: nmode=MODE_SETTLE;
		Steps.Sig0=PotDensity;
		Ballast.rho0=Steps.Sig0-10;  /* keep float surfaced */
		Settle.timeout=Steps.time0;
		Settle.seek_time=Settle.timeout;
		break;
	case 5: nmode=MODE_COMM;  /* Second Comm */
		commhome=1;  /* with homing */
		break;
	case 6: nmode=MODE_PROFILE_UP; /* surface after home */
		break;
	case 7: nmode=MODE_PROFILE_DOWN;
		save2=Down.Pmax;
		Down.Pmax=Ballast.MLtop;   /* Sample Sig0 at end of down */
		break;
	case 8: nmode=MODE_PROFILE_DOWN;
		Down.Pmax=save2;  /* restore Pmax */
		Steps.Sig0=PotDensity;  /* Get ML Density */
		log_event("ML Density %6.4f\n",Steps.Sig0);
		Down.Sigmax=Steps.Sig0+Steps.dSig3;  /* transit mixed layer to Deepest target */
		log_event("Down Target %6.4f\n",Down.Sigmax);
		break;
	case 9: nmode=MODE_SETTLE;
		Ballast.rho0=Steps.Sig0+Steps.dSig3;  /* first step */
		Settle.timeout=Steps.time3;
		Settle.seek_time=Settle.timeout;
		log_event("Settle Target %6.4f\n",Ballast.rho0);
		break;
	case 10: nmode=MODE_SETTLE;
		Ballast.rho0=Steps.Sig0+Steps.dSig2;  /* Second step */
		Settle.timeout=Steps.time2;
		Settle.seek_time=Settle.timeout;
		log_event("Settle Target %6.4f\n",Ballast.rho0);
		break;
	case 11: nmode=MODE_SETTLE;
		Ballast.rho0=Steps.Sig0+Steps.dSig1;  /* Third step */
		Settle.timeout=Steps.time1;
		Settle.seek_time=Settle.timeout;
		log_event("Settle Target %6.4f\n",Ballast.rho0);
		break;
	case 12: nmode=MODE_PROFILE_UP;    /* Profile up to Up.Pend */
		Up.Brate=save1;  /*Slow up */
		save10=Up.timeout;
		Up.timeout=100./(fabs(Up.Speed)+0.001);  /* long time */
		break;
	case 13: nmode=MODE_DRIFT_ISO;    /* drift in ML */
		Up.timeout=save10;
		break;
	default: nmode=MODE_ERROR;break;  /* can't get here */
}
printf("%d\n",nmode);
++icall;
return(nmode);
}
