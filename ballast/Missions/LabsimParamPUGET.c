/* lasbsimtest parameters for Puget Sound missions */
/* Do not change this variable */
static const char *__id__ = "$Id$";

    static double Zsettle=10.;  /* fluctuation in settle mode */
    static double WiggleZ=20.;  /* Tcline wiggle amplitude */
    static double WiggleT=2000.; /*  Period / sec */
    static double Tsettle=3000.;
    static double Wud=0.1;  /* up/down speed */
    static double Pml=3.;  /* mixed layer depth  */
    static double Wml=0.015;  /* mixed layer velocity */
    static double Daycalm=200;    /* Wind calms after this */
    static double Pcomm=15.;  /* pressure in Comm mode (end) */
    static double Wcalm=0.005; /* calm wind ML velocity */
    static double  UD= -1.;  /* up/down parameter in ML */
    static double Depth=150.;	/* Water depth */
    static double ml=0;  /* 1 for ML mission, 0 for T'cline mission */
    static double Sgoal=1;    /* Settle goal: 1 for downend, 0 for Pgoal */
    static double mlswitch=-1; /* subduct/obduct period <0 to void*/
    static double Tml=8.;       /* 24 for EQUATORIAL PACIFIC  */
    static double Sml=35.1;     /* 34.5 FOR EQUATORIAL PACIFIC */
    static double Sz= 0.001/50;	/* S gradient */
    static double Tz= -1/200.;  /* T gradient */
    static double Pgoal=60.;   /* 30 Tcline depth target */
    static double Tseek=8e3;  /* Tcline seek time*/
    static double Bgoal=150.e-6;  /* target for bocha */
    static double Brate= 1024./0.05/40000.;  /* rate to change Sml to get B to Bgoal */
