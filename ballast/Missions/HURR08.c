/* arch-tag: ed92ebd7-0589-42ed-9798-894b274fc4b0 */
/* HURRICANE 2008 MISSION 
ADD sampling() subroutine
*/

int next_mode(int nmode, double day_time)
{
double x;
static int icall=0;
static int cycle=16;

if (nmode==MODE_START){
	log_event("HURR08 MISSION\n");
	icall=0;
	nmode=MODE_PROFILE_DOWN;   /*initial down/up */
	Down.Pmax=120.;
	DISABLE_SENSORS(MODE_PROFILE_DOWN, SENS_ANR);
	DISABLE_SENSORS(MODE_PROFILE_UP, SENS_ANR); 
	return(nmode);
} 
else if (nmode==MODE_ERROR){ /* this should not happen since errors cause COMM */
	log_event("ERROR MODE at wierd time \n");
	return(nmode);  /* return with error */	
}
else if (nmode==MODE_COMM && icall%cycle !=2 ){  /* Error or Return from error */
log_event("Comm at wrong time - do nothing \n");
}

printf("NB Mode %d ->",nmode);

/* main mode loop - changes only stick until this routine is called again */
switch(icall%cycle){
	case 0: nmode=MODE_PROFILE_UP;break;
	case 1: nmode=MODE_COMM;break;
	case 2: nmode=MODE_SETTLE;   /* Settle0 - surface */
		   Settle.SetTarget=0;  /* don't autoset */
		   Ballast.Vset=0;  /* don't set Vol0 from surface */
		   Settle.Target=PotDensity-10;  /* stay on surface */
		   Settle.timeout=Steps.time0;
		   Settle.seek_time=Settle.timeout;
		break;
	case 3: nmode=MODE_SETTLE;  /* settle 1 - below MLB */
		   Ballast.Vset=1;  /* Vol0 set if good settle */
		   Steps.Sig0=PotDensity;  /* Reference surface density */
		   Settle.Target=Steps.Sig0+Steps.dSig1;  /* Just below ML */
		   Settle.timeout=Steps.time1;
		   Settle.seek_time=Settle.timeout;
		break;
	case 4: nmode=MODE_PROFILE_DOWN;    
		   Steps.z1=PressureG;   /* Reference pressure at end of previous settle*/
		   Ballast.Pgoal=Steps.z1+(Steps.z5-Steps.z1)/4;
		   Down.Pmax=Ballast.Pgoal;
		break;
	case 5: nmode=MODE_SETTLE;   /* Settle 2 - Tcline */
		   Settle.SetTarget=2;
		   Settle.timeout=Steps.time2;
		   Settle.seek_time=Settle.timeout;
		break;
	case 6: nmode=MODE_PROFILE_DOWN;    
		   Ballast.Pgoal=Steps.z1+(Steps.z5-Steps.z1)*2/4;
		   Down.Pmax=Ballast.Pgoal;
		break;
	case 7: nmode=MODE_SETTLE;   /* Settle 3 - Tcline */
		   Settle.timeout=Steps.time3;
		   Settle.seek_time=Settle.timeout;
		break;
	case 8: nmode=MODE_PROFILE_DOWN;    
		   Ballast.Pgoal=Steps.z1+(Steps.z5-Steps.z1)*3/4;
		   Down.Pmax=Ballast.Pgoal;
		break;
	case 9: nmode=MODE_SETTLE;   /* Settle 4 - Tcline */
		   Settle.timeout=Steps.time4;
		   Settle.seek_time=Settle.timeout;
		break;
	case 10: nmode=MODE_PROFILE_DOWN;    
		   Ballast.Pgoal=Steps.z5;
		   Down.Pmax=Ballast.Pgoal;
		break;
	case 11: nmode=MODE_SETTLE;   /* Settle 5 - bottom */
		   Settle.timeout=Steps.time5;
		   Settle.seek_time=Settle.timeout-2000;
		break;
	case 12: /* insert to middle of mixed layer */
		   Up.Pend=0.5*(Ballast.MLtop + Ballast.MLbottom);
		   nmode=MODE_PROFILE_UP;  
		break;
	case 13: /* first drift */
		nmode=MODE_DRIFT_ML;
		break;
	case 14: /* Intermediate COMM */
		nmode=MODE_COMM;
		break;
	case 15: nmode=MODE_DRIFT_ML;
		cycle=13; /* Profile next; skip drift mode; repeat profiles under satellite control */
		icall=-1;
		break;
	default: nmode=MODE_ERROR;break;  /* can't get here */
}
printf("%d\n",nmode);
++icall;
return(nmode);
}

#include "SampleHURR08.c"

