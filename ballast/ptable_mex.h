/*
**
*/
#ifndef _PTABLE_MEX_H_
#define _PTABLE_MEX_H_
# include <mex.h>

#define log_event printf
//#define log_event(x,...)


// type definitions from TT8 library

typedef unsigned char		uchar;
typedef volatile uchar		*ucpv;
typedef unsigned short		ushort;
typedef unsigned int		unsint;
typedef volatile ushort		*uspv;
typedef unsigned long		ulong;
typedef volatile ulong		*ulpv;
typedef char 				*ptr;
typedef	void				(*vfptr)(void);
// typedef enum {FALSE, TRUE}	bool;

////////////////// Hashtable definitions ////////////////////////////////////////////////

/*
** Constants which define whether the hash table keys are case sensitive
** (standard) or case insensitive.
*/
#define HT_STANDARD	0
#define HT_NOCASE	1

//typedef int (*ht_hash)(long base, const char *name);
typedef int (*ht_cmp)(const char *s1, const char *s2);

struct elem {
    unsigned short	len;
    void		*data;
    struct elem		*next;
    char		name[1];
};

typedef struct _hash_table {
    ht_cmp		cmp;
    struct elem		*first;
    struct elem		*last;
} HashTable;

typedef void (*ht_callback)(struct elem*, void*);

int ht_insert_elem(HashTable *htp, const char *name, const void *what,
		 unsigned short len);
int ht_find_elem(HashTable *htp, const char *name, void **whatp,
		   unsigned short *lenp);
void ht_remove_elem(HashTable *htp, const char *name);
HashTable *ht_create(unsigned size, int type);
void ht_destroy(HashTable *htp);
void ht_foreach_elem(HashTable *htp, ht_callback func, void *calldata);


/*
** Main PTABLE part
*/
#define NR_CELLS	51	/* Number of hash-table cells */

#define PTYPE_SHORT	0
#define PTYPE_LONG	1
#define PTYPE_DOUBLE	2
#define PTYPE_STRING	4
#define PTYPE_READ_ONLY	0x80
#define PTYPE_DIRTY	0x40
#define PTYPE_TYPEMASK	0x0f
#define PTYPE_SIZEMASK	0xff00
#define PTYPE_SIZESHIFT	8

#define PTYPE_GETSIZE(t)	(((t) & PTYPE_SIZEMASK) >> PTYPE_SIZESHIFT)
#define PTYPE_SETSIZE(t, n)	((t) | ((unsigned)(n) << PTYPE_SIZESHIFT))

struct param {
    unsigned	type;
    void	*loc;
};

void add_param(const char *name, int type, void *addr);
int params_read(const char *file); // this was originally found in xml_util.c
void dump_params(FILE *fp);
int init_param_table(void);
int get_param(const char *name, struct param *param);
long get_param_as_int(const char *name);
double get_param_as_double(const char *name);
int set_param_str(const char *name, const char *str);
int set_param_int(const char *name, long value);
int set_param_double(const char *name, double value);


mxArray* ptable2matlab(void); // returns mxArray pointer to a PTABLE structure

#endif /* _PTABLE_MEX_H_ */
