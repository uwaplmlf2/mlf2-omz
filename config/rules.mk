

.SUFFIXES:
.SUFFIXES: .S .c .coff .o .rhx .run .s
.PRECIOUS: %.coff

.s.o:
	$(COMPILE) -c $<

.S.o:
	$(COMPILE) -c $<

%.s: %.c
	$(COMPILE) -S $<

%.o: %.c
	@echo '$(COMPILE) -c $<'; \
	$(COMPILE) -Wp,-MD,.deps/$(*F).pp -c $<
	@-cp .deps/$(*F).pp .deps/$(*F).P; \
	tr ' ' '\012' < .deps/$(*F).pp \
	  | sed -e 's/^\\$$//' -e '/^$$/ d' -e '/:$$/ d' -e 's/$$/ :/' \
	    >> .deps/$(*F).P; \
	rm .deps/$(*F).pp

%.rhx: %.coff
	$(OBJCOPY)  -O srec -S $< $@

%.run: %.coff
	$(OBJCOPY) -O binary -S $< $@

.PHONY: clean subdirs $(SUBDIRS)
clean::
	rm -f *.o $(CLEANFILES)

ifdef ARCHIVE
.PHONY: archive $(ARCHIVE)
archive:: $(ARCHIVE)

$(ARCHIVE):
	$(ZIP) $(ZIPOPTS) $(ARCHIVE) $(DISTFILES)
endif

ifdef SUBDIRS
subdirs: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@
endif

DEPS_MAGIC := $(shell mkdir .deps > /dev/null 2>&1 || :)
-include $(DEP_FILES)
