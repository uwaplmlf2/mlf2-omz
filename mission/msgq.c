/*
** arch-tag: f8da6367-f255-482e-9251-a66797b07481
** Time-stamp: <2005-03-04 16:02:16 mike>
**
** Manage the queue of alert messages to be added to the status message.
**
*/
#include "config.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "log.h"
#include "msgq.h"


/**
 * mq_init - initialize the file queue data structures.
 */
void
mq_init(void)
{
}

/**
 * mq_add - add a message to the queue.
 */
void
mq_add(const char *text, ...)
{
    va_list	args;
    FILE	*ofp;

    va_start(args, text);
    if((ofp = fopen(MESSAGE_FILE, "a")) != NULL)
    {
	fprintf(ofp, "<alert>");
	vfprintf(ofp, text, args);
	fprintf(ofp, "</alert>");
	fclose(ofp);
    }
    
    va_end(args);
}
